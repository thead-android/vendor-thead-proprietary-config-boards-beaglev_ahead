ifeq ($(TARGET_PREBUILT_BOOTPART), )
LOCAL_BOOTPART := $(LOCAL_PATH)/bootpart.beaglev_ahead.ext4
else
LOCAL_BOOTPART := $(TARGET_PREBUILT_BOOTPART)
endif

ifeq ($(TARGET_PREBUILT_UBOOT),)
LOCAL_UBOOT := $(LOCAL_PATH)/u-boot-with-spl.bin
else
LOCAL_UBOOT := $(TARGET_PREBUILT_UBOOT)
endif

PRODUCT_COPY_FILES += \
    $(LOCAL_UBOOT):u-boot-with-spl.bin \
    $(LOCAL_BOOTPART):bootpart.ext4

# Peripherals config path
PERIPHERALS_CONFIG_PATH := vendor/xuantie/proprietary/config/peripherals

